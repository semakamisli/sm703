FROM openjdk:8
EXPOSE 8080
ADD target/sm703.jar sm703.jar
ENTRYPOINT ["java", "-jar", "/sm703.jar"]