package tr.edu.metu.sm703;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.Map;

import static java.util.Objects.isNull;

public class SM703Example implements RequestHandler<Map<String, Integer>, String> {

    public int add(Integer x, Integer y){
        return x+y;
    }

    @Override
    public String handleRequest(Map<String,Integer> input, Context context) {
        return "Response is:" + Integer.toString(add(input.get("x"), input.get("y")));
    }
}
