package tr.edu.metu.sm703;

import org.junit.jupiter.api.Test;

import static java.lang.Math.addExact;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class SM703ExampleTest {

    //private SM703Example sm703Example = new SM703Example();

    @Test
    public void testAdd(){
        final int expected = 3;
        final int result = addExact(2,1);

        assertEquals(expected, result);
    }

}
